from collections import Counter
from django.db.models import Count, When, Case, FloatField
from django.db.models.functions import Cast
from six.moves.urllib.request import urlopen
import time

import reverse_geocoder as rg
from instagram_private_api import Client

from utils.functions import send_get
from posts.models import Post, Media
from locations.models import Country, City
from .models import Profile
e


def get_client_api():
    api = Client('pitonist', 'insta7414darynd')
    return api


def get_user_id(username):
    url = f'https://www.instagram.com/web/search/topsearch/?query={username}'
    cookies = {
        'csrftoken': 'zwXJVEXGOHqghJ3a4kYfPYabKRfHnnHJ',
        'ig_did': 'DF09B2F1-09D9-4BA6-8317-CB0C06E88F7C',
        'mid': 'XxLsygAEAAFFu7Py5VV-ETm5P-zS',
        'sessionid': '14418983420%3AiLRCzSgmDSKU8p%3A23'
    }
    r = send_get(url, cookies=cookies)
    users = r.json()['users']

    for user in users:
        if user['user']['username'] == username:
            return user['user']['pk']
    else:
        raise Exception(f'{username} does not exists')


# def get_user_id(username):
#     link = 'http://www.instagram.com/' + username
#     response = urlopen(link)
#     content = str(response.read())
#     start_pos = content.find('"owner":{"id":"') + len('"owner":{"id":"')
#     end_pos = content[start_pos:].find('"')
#     insta_id = content[start_pos:start_pos+end_pos]
#     return int(insta_id)


def get_posts(client_api, user_id, count=18):
    api = client_api
    posts = api.user_feed(user_id)
    data = posts['items']

    if count > 18:
        while count > len(data):
            if posts.get('more_available'):
                kwargs = {'max_id': posts.get('next_max_id')}
                more_posts = api.user_feed(user_id, **kwargs)
                data += more_posts['items']
                time.sleep(1)
            else:
                break

    return data[:count]


def define_city_and_country_user(client_api, user_id):
    try:
        posts = get_posts(user_id=user_id, client_api=client_api)

        cities = []
        coordinates = []

        for post in posts:
            if post.get('location'):
                lng = post['location']['lng']
                lat = post['location']['lat']
                coordinates.append((lat, lng))

        geo_results = rg.search(coordinates, mode=1)

        for item in geo_results:
            cities.append(item['name'])

        cities_counter = Counter(cities)

        city = max(cities_counter, key=cities_counter.get)

        for item in geo_results:
            if item['name'] == city:
                country = item['cc']
                break
    except:
        return None, None

    return city, country


def get_followers(client_api, user_id):
    api = client_api
    all_followers = []

    rank_token = api.generate_uuid()
    followers = api.user_followers(user_id, rank_token)
    all_followers += [item['username'] for item in followers['users']]

    while followers.get('next_max_id'):
        data = {'max_id': followers['next_max_id']}
        followers = api.user_followers(user_id, rank_token, **data)
        all_followers += [item['username'] for item in followers['users']]

    return all_followers


def update_profile_base_info(client_api, user_id, profile):
    user_info = client_api.user_info(user_id)

    user_info = user_info['user']

    keys = {'full_name', 'is_private', 'profile_pic_url', 'media_count',
            'follower_count', 'following_count', 'biography', 'external_url',
            'total_igtv_videos', 'total_clips_count', 'total_ar_effects',
            'usertags_count', 'address_street', 'category', 'city_id',
            'city_name', 'contact_phone_number', 'latitude', 'longitude',
            'public_email', 'public_phone_country_code', 'public_phone_number',
            'instagram_location_id', 'is_business', 'account_type',
            'is_potential_business'}

    available_keys = {key for key in keys if key in user_info.keys()}

    data = {key: user_info[key] for key in user_info.keys() & available_keys}
    data['profile_id'] = user_info['pk']

    city, country = define_city_and_country_user(user_id=user_id, client_api=client_api)

    if country:
        country, _ = Country.objects.get_or_create(name=country)
    if city:
        city, _ = City.objects.get_or_create(country=country, name=city)

    profile.defined_city = city
    profile.__dict__.update(data)
    profile.save()
    return profile


def get_post_liked_users(client_api, post_id):
    api = client_api
    info = api.media_likers(post_id)
    return info['users']


def get_post_commented_users(client_api, post_id):
    api = client_api
    users = []
    comments = api.media_comments(post_id)

    if not comments['comments_disabled']:
        for comment in comments['comments']:
            users.append(comment['user'])

        while comments.get('next_max_id'):
            data = {'max_id': comments['next_max_id']}
            comments = api.media_comments(post_id, **data)

            for comment in comments['comments']:
                users.append(comment['user'])

    return users


def define_audience_location_city(profile):
    followers = profile.followers.all()
    cities_sum = City.objects.filter(profiles__in=followers).values('name').annotate(
        sum=Count('profiles')).order_by('-sum')
    followers_count = profile.followers.count() - profile.followers.filter(
        defined_city__isnull=True).count()

    for item in cities_sum:
        item['sum'] = (item['sum'] * 100) / followers_count

    return [i for i in cities_sum]


def define_audience_location_country(profile):
    followers = profile.followers.all()
    countries_sum = Country.objects.filter(cities__profiles__in=followers).values('name').annotate(
        sum=Count('cities__profiles')).order_by('-sum')
    followers_count = profile.followers.count() - profile.followers.filter(defined_city__isnull=True).count()

    for item in countries_sum:
        item['sum'] = (item['sum'] * 100) / followers_count

    return [i for i in countries_sum]


def save_post_liked_users(client_api, post):
    liked_users = get_post_liked_users(client_api, post.post_id)

    for user in liked_users:
        user_id = user['pk']
        username = user['username']
        liked_user_profile, created = Profile.objects.get_or_create(username=username)

        if created:
            liked_user_profile = update_profile_base_info(
                client_api=client_api,
                user_id=user_id,
                profile=liked_user_profile
            )
            time.sleep(2)

        post.liked_users.add(liked_user_profile)


def save_post_commented_users(client_api, post):
    commented_users = get_post_commented_users(client_api, post)

    for user in commented_users:
        user_id = user['pk']
        username = user['username']
        commented_user_profile, created = Profile.objects.get_or_create(username=username)

        if created:
            commented_user_profile = update_profile_base_info(
                client_api=client_api,
                user_id=user_id,
                profile=commented_user_profile
            )
            time.sleep(2)

        post.commented_users.add(commented_user_profile)


def save_posts(client_api, profile):
    posts = get_posts(client_api, profile.profile_id, count=100)

    for post in posts:
        new_post, _ = Post.objects.get_or_create(
            author=profile,
            post_id=post['pk'],
        )

        post['address'] = post['location']['address'] if post.get('location') else None
        post['city'] = post['location']['city'] if post.get('location') else None
        post['location_lng'] = post['location']['lng'] if post.get('location') else None
        post['location_lat'] = post['location']['lat'] if post.get('location') else None
        post['location'] = post['location']['name'] if post.get('location') else None
        post['text'] = post['caption']['text'] if post.get('caption') else None
        post['media_type'] = post['media_type'] if post['media_type'] in [1, 2, 8] else None

        keys = {'location', 'address', 'city', 'location_lng', 'location_lat',
                'text', 'media_type', 'code', 'can_viewer_reshare', 'caption_is_edited',
                'comment_likes_enabled', 'comment_threading_enabled', 'comment_count',
                'like_count', 'top_likers'}

        available_keys = {key for key in keys if key in post.keys()}

        data = {key: post[key] for key in post.keys() & available_keys}
        new_post.__dict__.update(data)
        new_post.save()

        if post['media_type'] == 1:
            Media.objects.create(
                post=new_post,
                photo=post['image_versions2']['candidates'][0]['url'],
            )
        elif post['media_type'] == 2:
            Media.objects.create(
                post=new_post,
                photo=post['image_versions2']['candidates'][0]['url'],
                video=post['video_versions'][0]['url'],
                view_count=post.get('view_count')
            )
        elif post['media_type'] == 8:
            for i in post['carousel_media']:
                if i['media_type'] == 1:
                    Media.objects.create(
                        post=new_post,
                        photo=i['image_versions2']['candidates'][0]['url']
                    )
                elif i['media_type'] == 2:
                    Media.objects.create(
                        post=new_post,
                        photo=i['image_versions2']['candidates'][0]['url'],
                        video=i['video_versions'][0]['url'],
                        view_count=i.get('view_count')
                    )

        save_post_liked_users(client_api=client_api, post=new_post)
        save_post_commented_users(client_api=client_api, post=new_post)

    commented_users_followers_gt_2000 = Post.objects.filter(author=profile).aggregate(
        count=Cast(Count(Case(When(
            commented_users__follower_count__gt=2000, then='commented_users'
        ))), FloatField()) * 100 / Count('commented_users', output_field=FloatField()))
    liked_users_followers_gt_2000 = Post.objects.filter(author=profile).aggregate(
        count=Cast(Count(Case(When(
            liked_users__follower_count__gt=2000, then='liked_users'
        ))), FloatField()) * 100 / Count('liked_users', output_field=FloatField()))
    commented_users_following_gt_2000 = Post.objects.filter(author=profile).aggregate(
        count=Cast(Count(Case(When(
            commented_users__following_count__gt=2000, then='commented_users'
        ))), FloatField()) * 100 / Count('commented_users', output_field=FloatField()))
    liked_users_following_gt_2000 = Post.objects.filter(author=profile).aggregate(
        count=Cast(Count(Case(When(
            liked_users__following_count__gt=2000, then='liked_users'
        ))), FloatField()) * 100 / Count('liked_users', output_field=FloatField()))
    commented_users_followers_lt_1000 = Post.objects.filter(author=profile).aggregate(
        count=Cast(Count(Case(When(
            commented_users__follower_count__lt=1000, then='commented_users'
        ))), FloatField()) * 100 / Count('commented_users', output_field=FloatField()))
    liked_users_followers_lt_1000 = Post.objects.filter(author=profile).aggregate(
        count=Cast(Count(Case(When(
            liked_users__follower_count__gt=1000, then='liked_users'
        ))), FloatField()) * 100 / Count('liked_users', output_field=FloatField()))

    profile.commented_users_followers_gt_2000 = commented_users_followers_gt_2000['count']
    profile.liked_users_followers_gt_2000 = liked_users_followers_gt_2000['count']
    profile.commented_users_following_gt_2000 = commented_users_following_gt_2000['count']
    profile.liked_users_following_gt_2000 = liked_users_following_gt_2000['count']
    profile.commented_users_followers_lt_1000 = commented_users_followers_lt_1000['count']
    profile.liked_users_followers_lt_1000 = liked_users_followers_lt_1000['count']
    profile.save()

    return posts


def get_profile_engagement_rate(client_api, profile):
    posts = get_posts(client_api, profile.profile_id, count=5)
    sum_likes_comments = 0

    for post in posts:
        sum_likes_comments += post['like_count']
        sum_likes_comments += post['comment_count']

    average = sum_likes_comments / len(posts)
    return average / profile.follower_count * 100


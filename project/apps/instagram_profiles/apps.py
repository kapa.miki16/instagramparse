from django.apps import AppConfig


class InstagramProfilesConfig(AppConfig):
    name = 'instagram_profiles'

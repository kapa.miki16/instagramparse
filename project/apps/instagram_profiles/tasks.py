import time

from config.celery import app

from .models import Profile
from .utils import (
    update_profile_base_info,
    get_client_api,
    get_user_id,
    define_audience_location_city,
    define_audience_location_country,
    save_posts
)


@app.task
def create_audience(followers, profile_uuid):
    api = get_client_api()
    profile = Profile.objects.get(uuid=profile_uuid)

    for f_username in followers:
        follower, created = Profile.objects.get_or_create(username=f_username)
        if created:
            follower_id = get_user_id(f_username)
            follower = update_profile_base_info(client_api=api, user_id=follower_id, profile=follower)
            time.sleep(2)
        profile.followers.add(follower)
    
    profile.audience_location_city = define_audience_location_city(profile)
    profile.audience_location_country = define_audience_location_country(profile)
    profile.save(update_fields=['audience_location_city', 'audience_location_country'])
    save_posts(api, profile)
    return {'success': True}
from django.urls import path
from .views import ProfileViewSet



urlpatterns = [
    path('parse/', ProfileViewSet.as_view({'post':'retrieve'})),
]
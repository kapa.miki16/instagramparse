from django.contrib import admin
from .models import Profile


@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    model = Profile
    list_display = (
        'username',
        'full_name',
        'profile_id',
        'follower_count',
        'following_count',
        'is_private'
    )
    list_filter = (
        'is_private',
    )
    search_fields = (
        'username',
    )
    fieldsets = (
        ('Основное', {
            'fields': (
                'profile_id',
                'username',
                'full_name',
                'defined_city',
                'is_private',
                'is_business',
                'account_type',
                'is_potential_business',
                'profile_pic_url',
                'media_count',
                'follower_count',
                'following_count',
                'biography',
                'external_url',
                'total_igtv_videos',
                'total_clips_count',
                'total_ar_effects',
                'usertags_count',
                'address_street',
                'category',
                'city_id',
                'city_name',
                'contact_phone_number',
                'latitude',
                'longitude',
                'public_email',
                'public_phone_country_code',
                'public_phone_number',
                'instagram_location_id',
                'engagement_rate',
                'activity',
                'followers',
                'audience_location_city',
                'audience_location_country'
            )
        }),
        ('Дополнительная информация', {
            'classes': ('collapse',),
            'fields': ('uuid', 'created_at', 'updated_at',)
        })
    )
    readonly_fields = ('uuid', 'created_at', 'updated_at')
    ordering = ('-created_at',)
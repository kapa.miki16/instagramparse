from rest_framework import serializers

from locations.serializers import CitySerializer
from .models import Profile


class ProfileSearchSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=256, required=True)


class ProfileSerializer(serializers.ModelSerializer):
    defined_city = CitySerializer()

    class Meta:
        model = Profile
        fields = (
            'uuid',
            'profile_id',
            'username',
            'full_name',
            'defined_city',
            'is_private',
            'engagement_rate',
            'is_business',
            'account_type',
            'is_potential_business',
            'profile_pic_url',
            'media_count',
            'follower_count',
            'following_count',
            'biography',
            'external_url',
            'total_igtv_videos',
            'total_clips_count',
            'total_ar_effects',
            'usertags_count',
            'address_street',
            'category',
            'city_id',
            'city_name',
            'contact_phone_number',
            'latitude',
            'longitude',
            'public_email',
            'public_phone_country_code',
            'public_phone_number',
            'instagram_location_id',
            'activity',
            'audience_location_city',
            'audience_location_country',
            'created_at',
            'updated_at'
        )

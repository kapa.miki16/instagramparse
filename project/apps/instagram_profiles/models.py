from django.db import models

from locations.models import City
from utils.models import DateModel


class Profile(DateModel):
    profile_id = models.BigIntegerField(
        verbose_name='ID инстаграма',
        blank=True,
        null=True
    )
    username = models.CharField(
        max_length=256,
        unique=True,
        verbose_name='Логин',
    )
    full_name = models.TextField(
        verbose_name='Full name',
        blank=True,
        null=True
    )
    is_private = models.BooleanField(
        default=False,
        blank=True,
        null=True
    )
    profile_pic_url = models.URLField(
        blank=True,
        null=True,
        max_length=666
    )
    media_count = models.PositiveBigIntegerField(
        blank=True,
        null=True
    )
    follower_count = models.PositiveBigIntegerField(
        verbose_name='Количество подписчиков',
        blank=True,
        null=True
    )
    following_count = models.PositiveBigIntegerField(
        verbose_name='Количество подписок',
        blank=True,
        null=True
    )
    biography = models.TextField(
        blank=True,
        null=True
    )
    external_url = models.URLField(
        blank=True,
        null=True,
        max_length=666
    )
    total_igtv_videos = models.PositiveBigIntegerField(
        blank=True,
        null=True
    )
    total_clips_count = models.PositiveBigIntegerField(
        blank=True,
        null=True
    )
    total_ar_effects = models.PositiveBigIntegerField(
        blank=True,
        null=True
    )
    usertags_count = models.PositiveBigIntegerField(
        blank=True,
        null=True
    )
    address_street = models.TextField(
        blank=True,
        null=True
    )
    category = models.TextField(
        blank=True,
        null=True
    )
    city_id = models.PositiveBigIntegerField(
        blank=True,
        null=True
    )
    city_name = models.TextField(
        blank=True,
        null=True
    )
    contact_phone_number = models.TextField(
        blank=True,
        null=True
    )
    latitude = models.BigIntegerField(
        blank=True,
        null=True
    )
    longitude = models.BigIntegerField(
        blank=True,
        null=True
    )
    public_email = models.EmailField(
        blank=True,
        null=True
    )
    public_phone_country_code = models.TextField(
        blank=True,
        null=True
    )
    public_phone_number = models.TextField(
        blank=True,
        null=True
    )
    instagram_location_id = models.TextField(
        blank=True,
        null=True
    )
    is_business = models.BooleanField(
        blank=True,
        null=True
    )
    account_type = models.PositiveBigIntegerField(
        blank=True,
        null=True
    )
    is_potential_business = models.BooleanField(
        blank=True,
        null=True
    )


    engagement_rate = models.PositiveBigIntegerField(
        verbose_name='Коэффициент вовлечённости',
        blank=True,
        null=True
    )
    activity = models.DecimalField(
        decimal_places=5,
        max_digits=10,
        blank=True,
        null=True,
        verbose_name='Активность'
    )
    defined_city = models.ForeignKey(
        City,
        related_name='profiles',
        on_delete=models.SET_NULL,
        blank=True,
        null=True
    )
    followers = models.ManyToManyField(
        'self',
        blank=True,
        null=True
    )
    audience_location_city = models.JSONField(
        blank=True,
        null=True
    )
    audience_location_country = models.JSONField(
        blank=True,
        null=True
    )
    commented_users_followers_gt_2000 = models.PositiveIntegerField(
        blank=True,
        null=True,
        verbose_name='Комментирующие с более чем 2000 подписчиками'
    )
    liked_users_followers_gt_2000 = models.PositiveIntegerField(
        blank=True,
        null=True,
        verbose_name='Лайкающие с более чем 2000 подписчиками'
    )
    commented_users_following_gt_2000 = models.PositiveIntegerField(
        blank=True,
        null=True,
        verbose_name='Комментирующие с более чем 2000 подписками'
    )
    liked_users_following_gt_2000 = models.PositiveIntegerField(
        blank=True,
        null=True,
        verbose_name='Лайкающие с более чем 2000 подписками'
    )
    commented_users_followers_lt_1000 = models.PositiveIntegerField(
        blank=True,
        null=True,
        verbose_name='Комментирующие с менее чем 1000 подписчиками'
    )
    liked_users_followers_lt_1000 = models.PositiveIntegerField(
        blank=True,
        null=True,
        verbose_name='Лайкающие с менее чем 1000 подписчиками'
    )

    class Meta:
        verbose_name = 'Аккаунт в инстаграме'
        verbose_name_plural = 'Аккаунты в инстаграме'

    def __str__(self):
        return self.username

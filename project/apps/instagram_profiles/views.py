from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from rest_framework import status
from .utils import (
    get_client_api,
    get_user_id,
    get_followers,
    update_profile_base_info,
    get_profile_engagement_rate
)
from .models import Profile
from .serializers import ProfileSearchSerializer, ProfileSerializer
from .tasks import create_audience


class ProfileViewSet(ModelViewSet):
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer

    def get_serializer_class(self):
        serializer_class = ProfileSerializer
        if self.action in ['retrieve', 'create']:
            serializer_class = ProfileSearchSerializer
        return serializer_class

    def retrieve(self, request, *args, **kwargs):
        serializer_class = self.get_serializer_class()
        serializer = serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        username = serializer.validated_data.get('username')

        try:
            user_id = get_user_id(username)
        except:
            return Response(status=status.HTTP_404_NOT_FOUND, data={'error': 'username not found'})

        profile, _ = Profile.objects.get_or_create(username=username)

        api = get_client_api()
        profile = update_profile_base_info(user_id=user_id, profile=profile, client_api=api)

        followers = get_followers(api, user_id)
        create_audience.delay(followers=followers, profile_uuid=str(profile.uuid))

        profile.engagement_rate = get_profile_engagement_rate(api, profile)
        profile.save(update_fields=['engagement_rate'])

        data = self.serializer_class(profile).data
        status_code = status.HTTP_200_OK

        return Response(status=status_code, data=data)

    # def profile(self, request, username):
    #     try:
    #         profile = Profile.objects.get(username=username)
    #     except:
    #         return Response(status=status.HTTP_404_NOT_FOUND, data={'error':'not found'})
    #
    #     serializengagement_rateer = self.serializer_class(profile)
    #     return Response(status=status.HTTP_200_OK, data=serializer.data)

    # def create(self, request, *args, **kwargs):
    #     serializer_class = self.get_serializer_class()
    #     serializer = serializer_class(data=request.data, many=True)
    #     serializer.is_valid(raise_exception=True)
    #     usernames = [item['username'] for item in serializer.validated_data]
    #
    #     if usernames:
    #         for username in usernames:
    #             async_get_data_instgram_profile.delay(username)
    #     return Response(status=status.HTTP_200_OK, data={'success': True})



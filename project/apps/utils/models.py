from uuid import uuid4
from django.db import models

# Create your models here.


class BaseModel(models.Model):
    uuid = models.UUIDField(
        primary_key=True,
        editable=False,
        default=uuid4
    )

    class Meta:
        abstract = True

    def __str__(self) -> str:
        return str(self.uuid)


class DateModel(BaseModel):
    created_at = models.DateTimeField(
        auto_now_add=True,
        verbose_name="дата создания"
    )
    updated_at = models.DateTimeField(
        auto_now=True,
        verbose_name="дата обновления"
    )

    class Meta:
        abstract = True

    def __str__(self) -> str:
        return str(self.uuid)

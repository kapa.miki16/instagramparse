import requests


def send_post(url, data, headers=None, params=None):
    r = requests.post(url, data=data, headers=headers, params=params)
    return r


def send_get(url, headers=None, params=None, cookies=None):
    r = requests.get(url, headers=headers, params=params, cookies=cookies)
    return r

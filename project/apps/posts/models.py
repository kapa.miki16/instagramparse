from django.db import models

from utils.models import DateModel
from instagram_profiles.models import Profile


class Post(DateModel):
    MEDIA_TYPE_CHOICES = (
        (1, 'Image'),
        (2, 'Video'),
        (8, 'Carousel')
    )
    author = models.ForeignKey(
        Profile,
        on_delete=models.CASCADE,
        related_name='posts'
    )
    post_id = models.PositiveBigIntegerField()
    code = models.CharField(
        max_length=256,
        blank=True,
        null=True
    )
    # location['name']
    location = models.CharField(
        max_length=256,
        blank=True,
        null=True
    )
    # location['address']
    address = models.CharField(
        max_length=256,
        blank=True,
        null=True
    )
    # location['city']
    city = models.CharField(
        max_length=256,
        blank=True,
        null=True
    )
    # location['lng']
    location_lng = models.PositiveBigIntegerField(
        blank=True,
        null=True
    )
    # location['lat']
    location_lat = models.PositiveBigIntegerField(
        blank=True,
        null=True
    )
    can_viewer_reshare = models.BooleanField(
        blank=True,
        null=True
    )
    caption_is_edited = models.BooleanField(
        blank=True,
        null=True
    )
    comment_likes_enabled = models.BooleanField(
        blank=True,
        null=True
    )
    comment_threading_enabled = models.BooleanField(
        blank=True,
        null=True
    )
    comment_count = models.PositiveBigIntegerField(
        blank=True,
        null=True
    )
    like_count = models.PositiveBigIntegerField(
        blank=True,
        null=True
    )
    top_likers = models.JSONField(
        blank=True,
        null=True
    )
    # caption['text']
    text = models.TextField(
        blank=True,
        null=True
    )
    media_type = models.PositiveSmallIntegerField(
        choices=MEDIA_TYPE_CHOICES,
        default=1,
        blank=True,
        null=True
    )
    liked_users = models.ManyToManyField(
        Profile,
        blank=True,
        null=True,
        related_name='liked_users'
    )
    commented_users = models.ManyToManyField(
        Profile,
        blank=True,
        null=True,
        related_name='commented_users'
    )

    class Meta:
        verbose_name = 'Пост'
        verbose_name_plural = 'Посты'

    def __str__(self):
        return str(self.post_id)


class Media(DateModel):
    post = models.ForeignKey(
        Post,
        on_delete=models.CASCADE,
    )
    # third['image_versions2']['candidates'][0]['url']
    # third['video_versions'][0]['url']
    photo = models.URLField(
        max_length=666,
        blank=True,
        null=True
    )
    video = models.URLField(
        max_length=666,
        blank=True,
        null=True
    )
    view_count = models.PositiveBigIntegerField(
        blank=True,
        null=True
    )

    # if third['media_type'] == 2:
    #     third['image_versions2']['candidates'][0]['url']
    #     third['video_versions'][0]['url']
    #     third['view_count']
    # if third['media_type'] == 1:
    #     third['image_versions2']['candidates'][0]['url']
    # else:
    #     for i in third['carousel_media']:
    #         if i['media_type'] == 2:
    #             i['image_versions2']['candidates'][0]['url']
    #             i['video_versions'][0]['url']
    #         if third['media_type'] == 1:
    #             i['image_versions2']['candidates'][0]['url']

    def __str__(self):
        return str(self.post.post_id)

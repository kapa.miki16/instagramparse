from django.contrib import admin
from .models import Post, Media


class MediaAdminInline(admin.TabularInline):
    model = Media
    extra = 0
    classes = ['collapse']


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display = (
        'author',
        'media_type',
        'post_id',
    )

    inlines = [MediaAdminInline]

    fieldsets = (
        ('Основное', {
            'fields': ('author', 'media_type', 'post_id', 'code')
        }),
        ('Данные', {
            'fields': (
                'text',
                'comment_count',
                'like_count',
                'can_viewer_reshare',
                'caption_is_edited',
                'comment_likes_enabled',
                'comment_threading_enabled',
                'top_likers',
                'liked_users'
            )
        }),
        ('Локация', {
            'fields': ('location', 'address', 'city', 'location_lng', 'location_lat')
        }),
        ('Дополнительная информация', {
            'classes': ('collapse',),
            'fields': ('uuid', 'created_at', 'updated_at',)
        })
    )

    readonly_fields = ('uuid', 'created_at', 'updated_at',)

    ordering = ('created_at', )
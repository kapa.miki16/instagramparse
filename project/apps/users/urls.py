from django.urls import path
from .views import UserViewSet



urlpatterns = [
    path('<str:phone>/', UserViewSet.as_view({'get': 'retrieve'}))
]
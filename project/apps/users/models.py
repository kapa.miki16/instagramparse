from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.db import models
from phonenumber_field.modelfields import PhoneNumberField
from utils.models import DateModel
from .managers import CustomUserManager


class CustomUser(DateModel, AbstractBaseUser, PermissionsMixin):
    GENDER_CHOICES = (
        (1, ('Муж')),
        (2, ('Жен')),
    )
    phone = PhoneNumberField(
        unique=True,
        verbose_name='Телефон',
    )
    email = models.EmailField(
        max_length=256,
        blank=True,
        null=True,
        verbose_name='Эмейл'
    )
    first_name = models.CharField(
        max_length=128,
        blank=True,
        verbose_name='Имя'
    )
    last_name = models.CharField(
        max_length=128,
        blank=True,
        verbose_name='Фамилия'
    )
    birthday = models.DateField(
        blank=True,
        null=True,
        verbose_name='Дата рождения'
    )
    gender = models.IntegerField(
        choices=GENDER_CHOICES,
        default=1,
        verbose_name='Пол'
    )
    avatar = models.ImageField(
        upload_to='uploads/avatars/',
        blank=True,
        verbose_name='Аватар',
        default='uploads/avatars/default.png'
    )
    is_staff = models.BooleanField(
        default=False,
        verbose_name='Сотрудник',
        help_text='Указывает, что пользователь имеет доступ к панели администратора с указанными правами',
    )
    is_active = models.BooleanField(
        default=True,
        verbose_name='Активен',
        help_text='Указывает, что пользователь имеет доступ к системе',
    )

    USERNAME_FIELD = 'phone'
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'

    def __str__(self):
        return str(self.phone)
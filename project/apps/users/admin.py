from django.contrib import admin
from .forms import CustomUserCreationForm, CustomUserChangeForm
from .models import CustomUser

admin.site.site_header = 'Instagram Parser'

@admin.register(CustomUser)
class CustomUserAdmin(admin.ModelAdmin):
    model = CustomUser
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    list_display = (
        'phone',
        'email',
        'is_staff',
        'is_active',
        'created_at',
        'updated_at',
    )
    list_filter = (
        'is_staff',
        'is_active',
    )
    search_fields = (
        'email',
        'phone',
        'last_name',
        'first_name',
    )
    fieldsets = (
        ('Основное', {
            'fields': (('phone',), ('email',), 'password',)
        }),
        ('Субъект', {
            'fields': ('last_name', 'first_name', 'birthday',)
        }),
        ('Права доступа', {
            'classes': ('collapse',),
            'fields': ('is_superuser', 'is_staff', 'is_active', 'groups', 'user_permissions',)
        }),
        ('Дополнительная информация', {
            'classes': ('collapse',),
            'fields': ('uuid', 'created_at', 'updated_at',)
        })
    )
    add_fieldsets = (
        ('Основное', {
            'classes': ('wide',),
            'fields': ('phone', 'password1', 'password2', 'is_staff', 'is_active', 'is_superuser')
        }),
        ('Субъект', {
            'classes': ('collapse',),
            'fields': ('uin', 'last_name', 'first_name', 'birthday',)
        })
    )
    readonly_fields = ('uuid', 'created_at', 'updated_at')
    ordering = ('created_at',)
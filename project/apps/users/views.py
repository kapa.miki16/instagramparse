from rest_framework.viewsets import ModelViewSet
from rest_framework import status
from rest_framework.response import Response

from .models import CustomUser
from .serializers import UserSerializer


class UserViewSet(ModelViewSet):
    queryset = CustomUser.objects.all()
    serializer_class = UserSerializer

    def retrieve(self, request, phone, *args, **kwargs):
        try:
            user = CustomUser.objects.get(phone=phone)
        except:
            return Response(status=status.HTTP_404_NOT_FOUND, data={'error': 'not found'})

        serializer = self.serializer_class(user)
        return Response(status=status.HTTP_200_OK, data=serializer.data)

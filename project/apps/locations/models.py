from django.db import models

from utils.models import DateModel


class Country(DateModel):
    name = models.CharField(
        max_length=256,
        verbose_name='Название'
    )

    class Meta:
        verbose_name = 'Страна'
        verbose_name_plural = 'Страны'

    def __str__(self):
        return self.name


class City(DateModel):
    country = models.ForeignKey(
        Country,
        on_delete=models.CASCADE,
        related_name='cities',
    )
    name = models.CharField(
        max_length=256,
        verbose_name='Название'
    )

    class Meta:
        verbose_name = 'Город'
        verbose_name_plural = 'Города'

    def __str__(self):
        return self.name

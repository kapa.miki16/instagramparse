from django.urls import path
from .views import ProfileViewSet



urlpatterns = [
    path('profiles/', ProfileViewSet.as_view({'post':'retrieve'})),
]

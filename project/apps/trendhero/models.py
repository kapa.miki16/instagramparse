from django.db import models

from utils.models import DateModel


class Profile(DateModel):
    ### USER INFO ###

    username = models.CharField(
        max_length=256,
        unique=True,
        verbose_name='Логин',
    )

    # ID
    profile_id = models.PositiveBigIntegerField(
        verbose_name='ID инстаграма',
        blank=True,
        null=True
    )
    connected_fb_page = models.BooleanField(
        blank=True,
        null=True
    )
    business_category_name = models.TextField(
        blank=True,
        null=True
    )
    overall_category_name = models.TextField(
        blank=True,
        null=True
    )
    profile_pic_url = models.URLField(
        blank=True,
        null=True,
        verbose_name='Аватарка',
        max_length=666
    )
    full_name = models.CharField(
        max_length=256,
        verbose_name='Full name',
        blank=True,
        null=True
    )
    external_url = models.URLField(
        blank=True,
        null=True,
        max_length=666
    )
    biography = models.TextField(
        blank=True,
        null=True
    )
    follower_count = models.PositiveBigIntegerField(
        blank=True,
        null=True
    )
    following_count = models.PositiveBigIntegerField(
        blank=True,
        null=True
    )
    media_count = models.PositiveBigIntegerField(
        blank=True,
        null=True
    )

    # TIMESTAMP
    last_post_at = models.DateTimeField(
        blank=True,
        null=True
    )
    highlight_reel_count = models.PositiveBigIntegerField(
        blank=True,
        null=True
    )
    er = models.PositiveBigIntegerField(
        blank=True,
        null=True
    )
    is_private = models.BooleanField(
        blank=True,
        null=True
    )
    is_verified = models.BooleanField(
        blank=True,
        null=True
    )
    has_profile_pic = models.BooleanField(
        blank=True,
        null=True
    )
    exists = models.BooleanField(
        blank=True,
        null=True
    )
    is_business_account = models.BooleanField(
        blank=True,
        null=True
    )
    is_joined_recently = models.BooleanField(
        blank=True,
        null=True
    )
    has_channel = models.BooleanField(
        blank=True,
        null=True
    )

    ### ['report']['report'] ###

    ### account_report ###
    country = models.TextField(
        blank=True,
        null=True
    )
    city = models.TextField(
        blank=True,
        null=True
    )
    language = models.TextField(
        blank=True,
        null=True
    )
    languages = models.JSONField(
        blank=True,
        null=True
    )
    topics = models.JSONField(
        blank=True,
        null=True
    )
    authentic_er = models.PositiveBigIntegerField(
        blank=True,
        null=True
    )

    ### audience_growth ###
    audience_growth = models.JSONField(
        blank=True,
        null=True
    )

    ### posts_report ###
    posts_report = models.JSONField(
        blank=True,
        null=True
    )

    ### audience_report ###
    audience_report = models.JSONField(
        blank=True,
        null=True
    )

    ### top_accounts_report ###
    top_accounts_report = models.JSONField(
        blank=True,
        null=True
    )

    class Meta:
        verbose_name = 'Аккаунт в инстаграме'
        verbose_name_plural = 'Аккаунты в инстаграме'

    def __str__(self):
        return self.username

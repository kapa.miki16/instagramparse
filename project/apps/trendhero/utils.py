import requests

from config.settings.extra import TRENDHERO_BASE_URL
from utils.functions import send_post, send_get


def trendhero_auth(data):
    url = f'{TRENDHERO_BASE_URL}api/user/session'
    r = send_post(url, data=data)
    return r


def trendhero_get_instagram_profile(username):
    url = f'{TRENDHERO_BASE_URL}api/reports/{username}'
    headers = {
        'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1OTk3MzA2NjYsInN1YiI6Mzc0MjV9.szjaGtdXBQaSSta_m1y2aIqe_z01UNiGE1xuLjCTnOk'
    }
    response = send_get(url, headers=headers)
    data = response.json()
    return data

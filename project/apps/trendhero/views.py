import datetime

from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from rest_framework import status

from instagram_profiles.serializers import ProfileSearchSerializer
from .utils import trendhero_get_instagram_profile
from .models import Profile
from .serializers import ProfileSerializer


class ProfileViewSet(ModelViewSet):
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer

    def get_serializer_class(self):
        serializer_class = ProfileSerializer
        if self.action in ['retrieve']:
            serializer_class = ProfileSearchSerializer
        return serializer_class

    def retrieve(self, request, *args, **kwargs):
        serializer_class = self.get_serializer_class()
        serializer = serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        username = serializer.validated_data.get('username')

        if self.queryset.filter(username=username):
            profile = self.queryset.get(username=username)
            data = ProfileSerializer(profile).data
            status_code = status.HTTP_200_OK
        else:
            try:
                to_create_data = {}

                data = trendhero_get_instagram_profile(username)

                if data.get('report'):
                    report = data['report']
                else:
                    report = data['report_preview']

                user_info = report['user_info']
                user_info['profile_id'] = user_info['id']
                del user_info['id']
                del user_info['cached_timestamp']

                last_post_date = user_info.get('last_post_at')
                if last_post_date:
                    user_info['last_post_at'] = datetime.datetime.fromtimestamp(last_post_date)
                # del user_info['last_post_at']
                account_report = report['report']['account_report']
                del account_report['status']

                audience_growth = {'audience_growth': report['report']['audience_growth']}
                posts_report = {'posts_report': report['report']['posts_report']}
                audience_report = {'audience_report': report['report']['audience_report']}
                top_accounts_report = {'top_accounts_report': report['report']['top_accounts_report']}

                to_create_data.update(user_info)
                to_create_data.update(account_report)
                to_create_data.update(audience_growth)
                to_create_data.update(posts_report)
                to_create_data.update(audience_report)
                to_create_data.update(top_accounts_report)

                profile = Profile.objects.create(**to_create_data)

                data = ProfileSerializer(profile).data
                status_code = status.HTTP_200_OK
            except Exception as error:
                data = {'error': str(error)}
                status_code = status.HTTP_400_BAD_REQUEST
        return Response(status=status_code, data=data)

    # def create(self, request, *args, **kwargs):
    #     serializer_class = self.get_serializer_class()
    #     serializer = serializer_class(data=request.data, many=True)
    #     serializer.is_valid(raise_exception=True)
    #     usernames = [item['username'] for item in serializer.validated_data]
    #
    #     if usernames:
    #         for username in usernames:
    #             async_get_data_instgram_profile.delay(username)
    #     return Response(status=status.HTTP_200_OK, data={'success': True})



from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi


schema_view = get_schema_view(
    openapi.Info(
        title="Instagram Profile Statistics API",
        default_version='v1',
        description="Test description",
        terms_of_service="https://www.google.com/policies/terms/",
        contact=openapi.Contact(email="darik_11.98@mail.ru"),
        license=openapi.License(name="BSD License"),
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

docpatterns = [
    path('', schema_view.without_ui(cache_timeout=0), name='api.docs.json'),
    path('redoc/', schema_view.with_ui('redoc', cache_timeout=0), name='api.docs.redoc'),
    path('swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='api.docs.swagger'),
]


apipatterns = [
    path('users/', include('users.urls')),
    path('profiles/', include('instagram_profiles.urls')),
    path('trendhero/', include('trendhero.urls')),
    path('documentation/', include(docpatterns))
]

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(apipatterns)),
]


if bool(settings.DEBUG):
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
# Python imports
import os

# fetch the common settings
from .common import *
from .extra import *

# ##### APPLICATION CONFIGURATION #########################

# allow all hosts during development
ALLOWED_HOSTS = ['*']


INSTALLED_APPS = DEFAULT_APPS
INSTALLED_APPS += [
    'corsheaders',
    'phonenumber_field',
    'rest_framework',
    'drf_yasg',
    #apps
    'users',
    'instagram_profiles',
    'trendhero',
    'locations',
    'posts'
]


MIDDLEWARE = [
    *MIDDLEWARE,
]


#### DATABASE CONFIGURATION ############################

# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.sqlite3',
#         'NAME': str(PROJECT_ROOT / 'run' / 'dev.sqlite3'),
#     }
# }
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'database_name',
        'USER': 'database_user',
        'PASSWORD': 'database_password',
        'HOST': 'localhost',
        'PORT': '5431',
    }
}


# ##### DEBUG CONFIGURATION ###############################
DEBUG = True


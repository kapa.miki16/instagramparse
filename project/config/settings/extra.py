### Cors ###
CORS_ORIGIN_ALLOW_ALL = True

### Celery ###
CELERY_BROKER_URL = 'redis://localhost:6379/0'
CELERY_RESULT_BACKEND = 'redis://localhost:6379/0'
CELERY_ACCEPT_CONTENT = ['application/json']
CELERY_RESULT_SERIALIZER = 'json'
CELERY_TASK_SERIALIZER = 'json'

### TrendHero ###
TRENDHERO_BASE_URL = 'https://api.trendhero.io/'

